#!/usr/bin/env python

import sys
from troubleshooterWizard import *
from PyQt4.QtGui import *
from PyQt4 import QtCore

def main():
    def back_button_pressed():
        # print("in back_button_pressed")  debug
        # the page switches so fast when the back button is pressed the ID is 3
        # by the time this is called.
        if wizard.currentId() == 3:
            # print("back pressed on page 4.")  debug
            wizard.back()

    qt_app = QApplication(sys.argv)
    wizard = Troubleshooter()
    wizard.show()
    back_button = wizard.button(QWizard.BackButton)
    QtCore.QObject.connect(back_button, QtCore.SIGNAL('clicked()'), back_button_pressed)
    # print(wizard.currentId())  debug
    return qt_app.exec_()

if __name__ == '__main__':
    main()
