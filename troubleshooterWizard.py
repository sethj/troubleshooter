#!/usr/bin/env python

from PyQt4 import QtCore
from PyQt4.QtGui import *

class IntroPage(QWizardPage):
    def __init__(self):
        QWizardPage.__init__(self)
        self.setTitle("Some more!")
        self.setSubTitle("Testing!")

        intro_text = "Some text! With a \n even!"

        self.introVBox = QVBoxLayout(self)

        self.label = QLabel(self)
        self.topSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.bottomSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.label.setText(intro_text)
        self.label.setAlignment(QtCore.Qt.AlignCenter)


        self.introVBox.addItem(self.topSpacer)
        self.introVBox.addWidget(self.label)
        self.introVBox.addItem(self.bottomSpacer)
        self.introVBox.setAlignment(QtCore.Qt.AlignVCenter)

class RecipePage(QWizardPage):
    def __init__(self):
        QWizardPage.__init__(self)
        self.setTitle("Select a recipe to run")
        self.setSubTitle("Curious about a recipe? Double click to see what it does.")

        self.recipeVBox = QVBoxLayout(self)
        self.recipeVBox.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.recipeTreeView = QTreeView(self)
        self.recipeVBox.addWidget(self.recipeTreeView)
        self.onlineRecipeCheckbox = QCheckBox(self)
        self.onlineRecipeCheckbox.setText("Include online recipes")
        self.onlineRecipeCheckbox.hide()
        self.recipeVBox.addWidget(self.onlineRecipeCheckbox)

class ProgressPage(QWizardPage):
    def __init__(self):
        QWizardPage.__init__(self)
        self.setTitle("Please wait")
        self.setSubTitle("The troubleshoter is retrieving relevant information about your system..")

        self.ProgressVBox = QVBoxLayout(self)

        self.topSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.bottomSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.progressBar = QProgressBar()
        self.progressBar.setProperty("value", 24)

        #self.ProgressVBox.addItem(self.topSpacer)
        self.ProgressVBox.addWidget(self.progressBar)
        self.ProgressVBox.addItem(self.bottomSpacer)

    def isComplete(self):
        #print("in isComplete") debug
        #if self.progressBar.value() < self.progressBar.maximum():
        #    return False
        #else:
        #    return True
	return True # debug

class FinishPage(QWizardPage):
    def __init__(self):
        QWizardPage.__init__(self)

        self.tabWidget = QTabWidget(self)
        self.summaryTab = QWidget()
        self.outputTab = QWidget()

        self.summaryVBox = QVBoxLayout(self)
        self.outputVBox = QVBoxLayout(self)

        self.outputTextBrowser = QTextBrowser(self)




class Troubleshooter(QWizard):
    Page_Intro = 1
    Page_Recipes = 2
    Page_Progress = 3
    Page_Finish = 4

    def __init__(self):
        QWizard.__init__(self)

        self.setPage(self.Page_Intro, IntroPage())
        self.setPage(self.Page_Recipes, RecipePage())
        self.setPage(self.Page_Progress, ProgressPage())
        self.setPage(self.Page_Finish, FinishPage())

        self.resize(545, 400)
