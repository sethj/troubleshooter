#!/usr/bin/env python

from gi.repository import Gtk
from core import Core


items = Core.get_recipes()

class ListView(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="ListView Testing")
        self.set_default_size(300, 230)
        self.set_border_width(10)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)

        listmodel = Gtk.ListStore(str, str)
        for i in items.keys():
            print i
            listmodel.append([i, items[i]])

        view = Gtk.TreeView(model=listmodel)
        cell = Gtk.CellRendererText()
        col = Gtk.TreeViewColumn("test", cell, text=0)
        col.set_resizable(True)
        col2 = Gtk.TreeViewColumn("desc.", cell, text=1)
        col2.set_resizable(True)
        view.append_column(col)
        view.append_column(col2)

        self.scrollableWindow = Gtk.ScrolledWindow()
        self.scrollableWindow.set_vexpand(True)
        vbox.pack_start(self.scrollableWindow, True, True, 0)
        self.scrollableWindow.add(view)

        checkbutton = Gtk.CheckButton("Click me!")
        vbox.pack_start(checkbutton, False, False, 5)


win = ListView()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
