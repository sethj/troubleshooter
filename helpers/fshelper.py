#!/usr/bin/python

import os

class FSHelper:

    homedir = os.getenv("HOME")
    configdir = ".troubleshooter/"

    loggerdir = ""
    recipedir = ""

    @staticmethod
    def __setup_home_dir(dotdir):
        try:
            os.mkdir(dotdir + "logs")
            os.mkdir(dotdir + "recipes")
            os.mkdir(dotdir + "cache")
            open(dotdir + "lock", 'a').close()
        except Exception as e:
            raise e
        FSHelper.loggerdir = dotdir + "logs/"
        FSHelper.recipedir = dotdir + "recipes/"

    @staticmethod
    def get_log_dir():
        if FSHelper.loggerdir == "":
            raise Exception("Logger directory not found")
        return FSHelper.recipedir

    @staticmethod
    def get_recipe_dir():
        if FSHelper.recipedir == "":
            raise Exception("Logger directory not found")
        return FSHelper.loggerdir

    @staticmethod
    def __setup_fs():
        dotdir = os.path.join(FSHelper.homedir, FSHelper.configdir)
        os.mkdir(dotdir)
        try:
            FSHelper.__setup_home_dir(dotdir)
        except Exception as e:
            raise e

    @staticmethod
    def initial_setup(force=False):
        # Has the user already set up the filesystem?
        if os.path.isfile(FSHelper.homedir + "/" + FSHelper.configdir + "lock") and not force:
            # return the directory
            return os.path.join(FSHelper.homedir, FSHelper.configdir)
        else:
            try:
                FSHelper.__setup_fs()
            except Exception as e:
                raise e
        return os.path.join(FSHelper.homedir, FSHelper.configdir)
